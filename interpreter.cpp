#include "interpreter.hpp"
#include "codeunit.hpp"
#include <cstring>
#include <iostream>

Interpreter::Interpreter() {
	// Do nothing.
}

std::vector <std::string> Interpreter::split(std::string str, std::string deli) {
	char* dup = strdup(str.c_str());
	std::vector <std::string> result;
	for (char* token = strtok(dup, deli.c_str()); token != NULL; token = strtok(NULL, deli.c_str())) {
		result.push_back(std::string(token));
	}
	return result;
}

bool Interpreter::prepareCode(std::string sourceText) {
	std::map <std::string, codetype> commands = {
			{"mall", mallt},
			{"set", sett},
			{"copy", copyt},
			{"writec", writect},
			{"readc", readct},
			{"writei", writeit},
			{"readi", readit},
			{"label", labelt},
			{"jmp", jmpt},
			{"jeq", jeqt},
			{"jne", jnet},
			{"jlt", jltt},
			{"jgt", jgtt},
			{"jle", jlet},
			{"jge", jget},
			{"inc", inct},
			{"dec", dect},
			{"add", addt},
			{"sub", subt},
			{"mul", mult},
			{"div", divt},
			{"ret", rett},
			{"def", deft}
		};
	this->code.clear();
	std::vector <std::string> source = this->split(sourceText, "\n\t;");
	for (unsigned int i = 0; i < source.size(); i++) {
		if (source[i][0] == '~') continue;
		source[i] = source[i].substr(0, source[i].find("~"));
		int sep = source[i].find_first_of(" ");
		std::string type = source[i].substr(0, sep);
		std::string args = source[i].substr(sep + 1);
		if (!commands.count(type)) {
			std::cerr << "Translation error: unknown command " << type << " at string " << i + 1 << ".\n";
			this->code.clear();
			return false;
		}
		if (commands[type] == labelt) {
			labels[args] = i;
		}
		if (commands[type] == deft) {
            std::vector <std::string> argsVector = split(args, " ");
            if (argsVector[0].find_first_not_of\
                ("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_-")\
                != std::string::npos) {
                    std::cerr << "Translation error: not valid constant name: '" << argsVector[0] << "'.\n";
                    return false;
                }
            if (argsVector.size() != 2) {
                std::cerr << "Translation error: invalid args for def: '" << args << "'.\n";
                return false;
            }
            defines[argsVector[0]] = argsVector[1];
		}
		this->code.push_back(makeUnit(commands[type], args, this));
	}
	return true;
}

int Interpreter::getCodeLen() {
	return this->code.size();
}

CodeUnit* Interpreter::getCodeAt(int i) {
	return this->code[i];
}

bool Interpreter::setCodePointer(unsigned int i) {
	if (i < code.size()) {
		this->codePointer = i;
		return true;
	} else {
		return false;
	}
}

unsigned int Interpreter::getCodePointer() {
	return codePointer;
}

int Interpreter::convertAddr(std::string sourceAddr) {
	char *failed;
	unsigned long int converted = strtoul(sourceAddr.c_str(), &failed, 10);
	if (*failed) {
		return -1;
	} else if (converted >= mem.size()) {
		return -2;
	}
	return (int) converted;
}

std::string Interpreter::replaceDefine(std::string arg) {
    if (defines.count(arg) != 0) return defines[arg];
    return arg;
}

bool Interpreter::setMemAt(int index, unsigned char value) {
	if ((unsigned int) index >= mem.size()) return false;
	mem[index] = value;
	return true;
}

unsigned char Interpreter::getMemAt(int index) {
	return mem[index];
}

int Interpreter::getLabelAddr(std::string label) {
	if (!labels.count(label)) return -1;
	return labels[label];
}

void Interpreter::addMemory(long int count) {
	mem.resize(mem.size() + count);
}

void Interpreter::setReturnCode(int value) {
    returnCode = value;
}

int Interpreter::getReturnCode() {
    return returnCode;
}

bool Interpreter::step() {
	if (codePointer >= code.size()) return false;
	CodeUnit *what = code[codePointer];
	unsigned int oldPointer = codePointer;
	bool result = what->execute();
	if (oldPointer == codePointer) {
		codePointer++;
	}
	return result;
}

#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <sstream>
#include "codeunit.hpp"
#include "interpreter.hpp"

using std::cin;
using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::ifstream;
using std::ofstream;
using std::stringstream;

int main(int argc, char** argv) {
	ofstream logfile;
	logfile.open("log.txt");
	ifstream file;
	if (argc > 1) {
		file.open(argv[1]);
	} else {
		file.open("test.abf");
	}
	if (!file.is_open()) return 128;
	stringstream tmp;
	tmp << file.rdbuf();
	string source = tmp.str();
	Interpreter ipr;
	if (ipr.prepareCode(source)) {
		logfile << "Well, that's your code (" << ipr.getCodeLen() << " units):" << endl;
		for (int i = 0; i < ipr.getCodeLen(); i++) {
			logfile << ipr.getCodeAt(i)->getDebugString() << endl;
		}
	} else {
		logfile << "Translation error!" << endl;
		logfile.close();
		file.close();
		return 1;
	}
	logfile << "Let's execute it!" << endl;
	int counter;
	for (counter = 0; ipr.step(); counter++);
	logfile << "Program stopped at " << counter << " operation, " << ipr.getCodePointer() << " unit.";
	logfile.close();
	file.close();
	return ipr.getReturnCode();
}

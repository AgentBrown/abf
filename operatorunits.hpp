#ifndef OPERATORUNITS_HPP
#define OPERATORUNITS_HPP

#include "codeunit.hpp"

class OperatorUnit : public CodeUnit {
	public:
		OperatorUnit(codetype, std::string, Interpreter*);
		virtual bool execute();
		virtual unsigned char doOperation(unsigned char, unsigned char);
};

class AddUnit : public OperatorUnit {
	public:
		AddUnit(codetype, std::string, Interpreter*);
		virtual unsigned char doOperation(unsigned char, unsigned char);
};

class SubUnit : public OperatorUnit {
	public:
		SubUnit(codetype, std::string, Interpreter*);
		virtual unsigned char doOperation(unsigned char, unsigned char);
};

class MulUnit : public OperatorUnit {
	public:
		MulUnit(codetype, std::string, Interpreter*);
		virtual unsigned char doOperation(unsigned char, unsigned char);
};

class DivUnit : public OperatorUnit {
	public:
		DivUnit(codetype, std::string, Interpreter*);
		virtual unsigned char doOperation(unsigned char, unsigned char);
};

#endif

#include <iostream>
#include <algorithm>
#include "codeunit.hpp"
#include "simpleunits.hpp"
#include "operatorunits.hpp"
#include "jumpunits.hpp"
#include "interpreter.hpp"

CodeUnit::CodeUnit(codetype argctype = mallt, std::string argargs = "0", Interpreter *argparent = NULL) {
	this->ctype = argctype;
	this->args = argargs;
	this->parent = argparent;
}

std::string CodeUnit::dereferenceArg(std::string arg) {
    if (arg == "") return "";
    arg = this->parent->replaceDefine(arg);
    if (arg[0] == '[' && arg.back() == ']') {
        int addr = this->parent->convertAddr(dereferenceArg(arg.substr(1, arg.size() - 2)));
        if (addr == -1) {
            return "";
        }
        if (addr == -2) {
            return "";
        }
        return std::to_string((unsigned int) this->parent->getMemAt(addr));
    } else {
        return arg;
    }
}

bool CodeUnit::processArgs() {
    std::vector<std::string> argsArray = this->parent->split(this->args, " ");
    std::string result = "";
    for (unsigned int i = 0; i < argsArray.size(); i++) {
        std::string ready = dereferenceArg(argsArray[i]);
        if (ready == "") return false;
        result += ready + " ";
    }
    this->args = result.substr(0, result.size() - 1); // Remove last space.
    return true;
}

codetype CodeUnit::getType() {
	return this->ctype;
}

std::string CodeUnit::getArgs() {
	return this->args;
}

std::string CodeUnit::getDebugString() {
	std::string result = "<CodeUnit of type ";
	result += std::to_string(this->ctype) + " (" + debugCommandText[this->ctype] +\
											") with args '" + this->args + "'>";
	return result;
}

bool CodeUnit::execute() {
    this->processArgs();
	return true;
}

CodeUnit* makeUnit(codetype ct, std::string args, Interpreter *parent) {
	switch (ct) {
		case mallt: {
			return new MallUnit(ct, args, parent);
			break;
		}
		case sett: {
			return new SetUnit(ct, args, parent);
			break;
		}
		case copyt: {
			return new CopyUnit(ct, args, parent);
			break;
		}
		case writect: {
			return new WritecUnit(ct, args, parent);
			break;
		}
		case readct: {
			return new ReadcUnit(ct, args, parent);
			break;
		}
		case writeit: {
			return new WriteiUnit(ct, args, parent);
			break;
		}
		case readit: {
			return new ReadiUnit(ct, args, parent);
			break;
		}
		case jmpt: {
			return new JmpUnit(ct, args, parent);
			break;
		}
		case inct: {
			return new IncUnit(ct, args, parent);
			break;
		}
		case dect: {
			return new DecUnit(ct, args, parent);
			break;
		}
		case jeqt: {
			return new JeqUnit(ct, args, parent);
			break;
		}
		case jnet: {
			return new JneUnit(ct, args, parent);
			break;
		}
		case jgtt: {
			return new JgtUnit(ct, args, parent);
			break;
		}
		case jltt: {
			return new JltUnit(ct, args, parent);
			break;
		}
		case jget: {
			return new JgeUnit(ct, args, parent);
			break;
		}
		case jlet: {
			return new JleUnit(ct, args, parent);
			break;
		}
		case addt: {
			return new AddUnit(ct, args, parent);
			break;
		}
		case subt: {
			return new SubUnit(ct, args, parent);
			break;
		}
		case mult: {
			return new MulUnit(ct, args, parent);
			break;
		}
		case divt: {
			return new DivUnit(ct, args, parent);
			break;
		}
		case rett: {
            return new RetUnit(ct, args, parent);
            break;
		}
		default: break;
	}
	return new CodeUnit(ct, args, parent);
}

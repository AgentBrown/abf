# ABF #

This is the test programing language, please ignore.

To install interpreter do "make" in repository catalog and copy "abfi" file wherever you want.

Operations, that u can use:


```
#!

mall <num>                                      ::: add <num> memory
def <str> <expr>                                ::: in arguments to any operator replace <str> to <expr>, [<str>] to [<expr>] and so on
set <addr> <num>                                ::: write <num> to <addr>
copy <addr1> <addr2>							::: copy data from <addr1> to <addr2>
writec <addr>                                   ::: write data from <addr> as character
readc <addr>                                    ::: read character data to <addr>
writei <addr>									::: write data from <addr> as integer
readi <addr>									::: read integer data to <addr>
label <str>                                     ::: set label named <str> here
jmp <str>                                       ::: go to label named <str>
j{eq,ne,gt,lt,ge,le} <addr1> <addr2> <str>      ::: jump to label <str> if value at <addr1> {==,!=,>,<,>=,<=} value at <addr2>
inc <addr>                                      ::: increment value at address <addr>
dec <addr>                                      ::: decrement value at address <addr>
{add,sub,mul,div} <addr1> <addr2>				::: <addr1> {+,-,*,/}= <addr2>
ret <num>                                       ::: exit program and return <num>
```


U can divide operations with newlines or semicolons.
All text after '~' and before newline or semicolon is comment and will be ignored.
In any arg (including string args, but will be serious problems) u can type [<addr>] instead of arg. Then preprocessor will replace it to data from <addr>.
First arg of def must match regex ^[a-zA-Z0-9_-]+$.
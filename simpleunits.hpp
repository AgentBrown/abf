#ifndef SIMPLEUNITS_HPP
#define SIMPLEUNITS_HPP

#include "codeunit.hpp"
#include "interpreter.hpp"

class MallUnit : public CodeUnit {
	public:
		MallUnit(codetype, std::string, Interpreter*);
		virtual bool execute();

};

class ReadiUnit : public CodeUnit {
	public:
		ReadiUnit(codetype, std::string, Interpreter*);
		virtual bool execute();
};

class WriteiUnit : public CodeUnit {
	public:
		WriteiUnit(codetype, std::string, Interpreter*);
		virtual bool execute();
};

class ReadcUnit : public CodeUnit {
	public:
		ReadcUnit(codetype, std::string, Interpreter*);
		virtual bool execute();
};

class WritecUnit : public CodeUnit {
	public:
		WritecUnit(codetype, std::string, Interpreter*);
		virtual bool execute();
};

class JmpUnit : public CodeUnit {
	public:
		JmpUnit(codetype, std::string, Interpreter*);
		virtual bool execute();
};

class IncUnit : public CodeUnit {
	public:
		IncUnit(codetype, std::string, Interpreter*);
		virtual bool execute();
};

class DecUnit : public CodeUnit {
	public:
		DecUnit(codetype, std::string, Interpreter*);
		virtual bool execute();
};

class SetUnit : public CodeUnit {
	public:
		SetUnit(codetype, std::string, Interpreter*);
		virtual bool execute();
};

class CopyUnit : public CodeUnit {
	public:
		CopyUnit(codetype, std::string, Interpreter*);
		virtual bool execute();
};

class RetUnit : public CodeUnit {
    public:
        RetUnit(codetype, std::string, Interpreter*);
        virtual bool execute();
};

#endif

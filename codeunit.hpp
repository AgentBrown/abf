#ifndef CODEUNIT_HPP
#define CODEUNIT_HPP

#include <string>
#include <map>

class Interpreter;

enum codetype {
	mallt,
	sett,
	copyt,
	writect,
	readct,
	writeit,
	readit,
	labelt,
	jmpt,
	jeqt,
	jnet,
	jgtt,
	jltt,
	jlet,
	jget,
	inct,
	dect,
	addt,
	subt,
	mult,
	divt,
	rett,
	deft
};

class CodeUnit {
	protected:
		codetype ctype;
		std::string args;
		std::map <codetype, std::string> debugCommandText = {
			{mallt, "mall"},
			{sett, "set"},
			{copyt, "copy"},
			{writect, "writec"},
			{readct, "readc"},
			{readit, "readi"},
			{labelt, "label"},
			{jmpt, "jmp"},
			{inct, "inc"},
			{jeqt, "jeq"},
			{jnet, "jne"},
			{jltt, "jlt"},
			{jgtt, "jgt"},
			{jlet, "jle"},
			{jget, "jge"},
			{dect, "dec"},
			{addt, "add"},
			{subt, "sub"},
			{mult, "mul"},
			{divt, "div"},
			{writeit, "writei"},
			{rett, "ret"},
			{deft, "def"}
		};
		Interpreter *parent;
		bool processArgs();
		std::string dereferenceArg(std::string);
	public:
		CodeUnit(codetype, std::string, Interpreter*);
		codetype getType();
		std::string getArgs();
		std::string getDebugString();
		virtual bool execute();
};

CodeUnit* makeUnit(codetype, std::string, Interpreter*);

#endif

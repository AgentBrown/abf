#include <iostream>
#include "jumpunits.hpp"

BaseJumpUnit::BaseJumpUnit(codetype ct, std::string a, Interpreter* p) : CodeUnit::CodeUnit(ct, a, p) {}
bool BaseJumpUnit::execute() {
    CodeUnit::execute();
    std::vector <std::string> args = this->parent->split(this->args, " ");
	if (args.size() != 3) {
		std::cerr << "Runtime error: invalid args for jeq: '" << this->args << "'.\n";
		return false;
	}
	for (unsigned int i = 0; i < args.size() - 1; i++) {
		if (this->parent->convertAddr(args[i]) == -1) {
			std::cerr << "Runtime error: invalid arg for jeq: '"  << args[i] << "'.\n";
			return false;
		} else if (this->parent->convertAddr(args[i]) == -2) {
			std::cerr << "Runtime error: out of allocated range.\n";
			return false;
		}
	}
	if (this->decide(this->parent->getMemAt(stoi(args[0])), this->parent->getMemAt(stoi(args[1])))) {
		long int addr = this->parent->getLabelAddr(args[2]);
		if (addr == -1) {
			std::cerr << "Runtime error: no such label: '" << args[2] << "'.\n";
			return false;
		}
		this->parent->setCodePointer(addr);
		return true;
	}
	return true;
}

bool BaseJumpUnit::decide(unsigned char left, unsigned char right) {
    return true;
}

JeqUnit::JeqUnit(codetype ct, std::string a, Interpreter *p) : BaseJumpUnit::BaseJumpUnit(ct, a, p) {}
bool JeqUnit::decide(unsigned char left, unsigned char right) {
    return left == right;
}

JneUnit::JneUnit(codetype ct, std::string a, Interpreter *p) : BaseJumpUnit::BaseJumpUnit(ct, a, p) {}
bool JneUnit::decide(unsigned char left, unsigned char right) {
    return left != right;
}

JltUnit::JltUnit(codetype ct, std::string a, Interpreter *p) : BaseJumpUnit::BaseJumpUnit(ct, a, p) {}
bool JltUnit::decide(unsigned char left, unsigned char right) {
    return left < right;
}

JgtUnit::JgtUnit(codetype ct, std::string a, Interpreter *p) : BaseJumpUnit::BaseJumpUnit(ct, a, p) {}
bool JgtUnit::decide(unsigned char left, unsigned char right) {
    return left > right;
}

JleUnit::JleUnit(codetype ct, std::string a, Interpreter *p) : BaseJumpUnit::BaseJumpUnit(ct, a, p) {}
bool JleUnit::decide(unsigned char left, unsigned char right) {
    return left <= right;
}

JgeUnit::JgeUnit(codetype ct, std::string a, Interpreter *p) : BaseJumpUnit::BaseJumpUnit(ct, a, p) {}
bool JgeUnit::decide(unsigned char left, unsigned char right) {
    return left >= right;
}

CC=g++
CFLAGS=-c -g -Wall --std=c++11
LDFLAGS=
SOURCES=codeunit.cpp interpreter.cpp abfi.cpp simpleunits.cpp operatorunits.cpp jumpunits.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=abfi
CC_COMMAND=$(CC) $(CFLAGS) -o $@ $<

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

%.o: %.cpp %.hpp
	$(CC_COMMAND)

%.o: %.cpp
	$(CC_COMMAND)

clean:
	rm -f *.o

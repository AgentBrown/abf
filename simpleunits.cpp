#include <iostream>
#include <vector>
#include "simpleunits.hpp"
#include "interpreter.hpp"

MallUnit::MallUnit(codetype ct, std::string args, Interpreter* p) : CodeUnit::CodeUnit(ct, args, p) {}
bool MallUnit::execute() {
    CodeUnit::execute();
	char *failed;
	long int converted = strtol(this->args.c_str(), &failed, 10);
	if (*failed) {
		std::cerr << "Runtime error: incorrect argument for mall: '" << this->args << "'.\n";
		return false;
	} else {
		this->parent->addMemory(converted);
		return true;
	}
}

ReadiUnit::ReadiUnit(codetype ct, std::string args, Interpreter* p) : CodeUnit::CodeUnit(ct, args, p) {}
bool ReadiUnit::execute() {
    CodeUnit::execute();
	long int addr = this->parent->convertAddr(this->args);
	if (addr == -1) {
		std::cerr << "Runtime error: invalid arg for readi: '"  << this->args << "'.\n";
		return false;
	} else if (addr == -2) {
		std::cerr << "Runtime error: out of allocated range.\n";
		return false;
	}
	int t;
	std::cin >> t;
	this->parent->setMemAt(addr, (unsigned char) t);
	return true;
}

WriteiUnit::WriteiUnit(codetype ct, std::string args, Interpreter* p) : CodeUnit::CodeUnit(ct, args, p) {}
bool WriteiUnit::execute() {
    CodeUnit::execute();
	long int addr = this->parent->convertAddr(this->args);
	if (addr == -1) {
		std::cerr << "Runtime error: invalid arg for writei: '"  << this->args << "'.\n";
		return false;
	} else if (addr == -2) {
		std::cerr << "Runtime error: out of allocated range.\n";
		return false;
	}
	std::cout << (int) this->parent->getMemAt(addr);
	return true;
}

ReadcUnit::ReadcUnit(codetype ct, std::string args, Interpreter* p) : CodeUnit::CodeUnit(ct, args, p) {}
bool ReadcUnit::execute() {
    CodeUnit::execute();
	long int addr = this->parent->convertAddr(this->args);
	if (addr == -1) {
		std::cerr << "Runtime error: invalid arg for readi: '"  << this->args << "'.\n";
		return false;
	} else if (addr == -2) {
		std::cerr << "Runtime error: out of allocated range.\n";
		return false;
	}
	unsigned char t;
	std::cin >> t;
	this->parent->setMemAt(addr, t);
	return true;
}

WritecUnit::WritecUnit(codetype ct, std::string args, Interpreter* p) : CodeUnit::CodeUnit(ct, args, p) {}
bool WritecUnit::execute() {
    CodeUnit::execute();
	long int addr = this->parent->convertAddr(this->args);
	if (addr == -1) {
		std::cerr << "Runtime error: invalid arg for writei: '"  << this->args << "'.\n";
		return false;
	} else if (addr == -2) {
		std::cerr << "Runtime error: out of allocated range.\n";
		return false;
	}
	std::cout << this->parent->getMemAt(addr);
	return true;
}

JmpUnit::JmpUnit(codetype ct, std::string args, Interpreter* p) : CodeUnit::CodeUnit(ct, args, p) {}
bool JmpUnit::execute() {
    CodeUnit::execute();
	long int addr = this->parent->getLabelAddr(this->args);
	if (addr == -1) return false;
	this->parent->setCodePointer(addr);
	return true;
}

IncUnit::IncUnit(codetype ct, std::string args, Interpreter* p) : CodeUnit::CodeUnit(ct, args, p) {}
bool IncUnit::execute() {
    CodeUnit::execute();
	long int addr = this->parent->convertAddr(this->args);
	if (addr == -1) {
		std::cerr << "Runtime error: invalid arg for inc: '"  << this->args << "'.\n";
		return false;
	} else if (addr == -2) {
		std::cerr << "Runtime error: out of allocated range.";
	}
	return this->parent->setMemAt(addr, this->parent->getMemAt(addr) + 1);
}

DecUnit::DecUnit(codetype ct, std::string args, Interpreter* p) : CodeUnit::CodeUnit(ct, args, p) {}
bool DecUnit::execute() {
    CodeUnit::execute();
	long int addr = this->parent->convertAddr(this->args);
	if (addr == -1) {
		std::cerr << "Runtime error: invalid arg for inc: '"  << this->args << "'.\n";
		return false;
	} else if (addr == -2) {
		std::cerr << "Runtime error: out of allocated range.";
	}
	return this->parent->setMemAt(addr, this->parent->getMemAt(addr) - 1);
}

SetUnit::SetUnit(codetype ct, std::string args, Interpreter* p) : CodeUnit::CodeUnit(ct, args, p) {}
bool SetUnit::execute() {
    CodeUnit::execute();
	std::vector <std::string> args = this->parent->split(this->args, " ");
	if (args.size() != 2) {
		std::cerr << "Runtime error: invalid args for set: '" <<  this->args << "'.\n";
		return false;
	}
	long int addr = this->parent->convertAddr(args[0]);
	if (addr == -1) {
		std::cerr << "Runtime error: invalid arg for set: '"  << args[0] << "'.\n";
		return false;
	} else if (addr == -2) {
		std::cerr << "Runtime error: out of allocated range.";
	}
	char *failed;
	long int value = strtol(args[1].c_str(), &failed, 10);
	if (*failed) {
		std::cerr << "Runtime error: invalid arg for set: '"  << args[1] << "'.\n";
		return false;
	}
	this->parent->setMemAt(addr, value);
	return true;
}

CopyUnit::CopyUnit(codetype ct, std::string args, Interpreter* p) : CodeUnit::CodeUnit(ct, args, p) {}
bool CopyUnit::execute() {
    CodeUnit::execute();
	std::vector <std::string> args = this->parent->split(this->args, " ");
	if (args.size() != 2) {
		std::cerr << "Runtime error: invalid args for copy: '" << this->args << "'.\n";
		return false;
	}
	long int addr1 = this->parent->convertAddr(args[0]);
	if (addr1 == -1) {
		std::cerr << "Runtime error: invalid arg for copy: '"  << args[0] << "'.\n";
		return false;
	} else if (addr1 == -2) {
		std::cerr << "Runtime error: out of allocated range.\n";
		return false;
	}
	long int addr2 = this->parent->convertAddr(args[0]);
	if (addr1 == -1) {
		std::cerr << "Runtime error: invalid arg for copy: '"  << args[0] << "'.\n";
		return false;
	} else if (addr2 == -2) {
		std::cerr << "Runtime error: out of allocated range.\n";
		return false;
	}
	this->parent->setMemAt(addr1, this->parent->getMemAt(addr2));
	return true;
}

RetUnit::RetUnit(codetype ct, std::string args, Interpreter* p) : CodeUnit::CodeUnit(ct, args, p) {}
bool RetUnit::execute() {
    CodeUnit::execute();
    char *failed;
    long int value = strtol(args.c_str(), &failed, 10);
	if (*failed) {
		std::cerr << "Runtime error: invalid arg for ret: '"  << args << "'.\n";
		return false;
	}
	this->parent->setReturnCode(value);
	return false;
}

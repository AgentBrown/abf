#include "codeunit.hpp"
#include "interpreter.hpp"

class BaseJumpUnit : public CodeUnit {
public:
    BaseJumpUnit(codetype, std::string, Interpreter*);
    virtual bool execute();
    virtual bool decide(unsigned char, unsigned char);
};


class JeqUnit : public BaseJumpUnit {
	public:
		JeqUnit(codetype, std::string, Interpreter*);
		virtual bool decide(unsigned char, unsigned char);
};

class JneUnit : public BaseJumpUnit {
	public:
		JneUnit(codetype, std::string, Interpreter*);
		virtual bool decide(unsigned char, unsigned char);
};

class JgtUnit : public BaseJumpUnit {
	public:
		JgtUnit(codetype, std::string, Interpreter*);
		virtual bool decide(unsigned char, unsigned char);
};

class JltUnit : public BaseJumpUnit {
	public:
		JltUnit(codetype, std::string, Interpreter*);
		virtual bool decide(unsigned char, unsigned char);
};

class JleUnit : public BaseJumpUnit {
	public:
		JleUnit(codetype, std::string, Interpreter*);
		virtual bool decide(unsigned char, unsigned char);
};

class JgeUnit : public BaseJumpUnit {
	public:
		JgeUnit(codetype, std::string, Interpreter*);
		virtual bool decide(unsigned char, unsigned char);;
};

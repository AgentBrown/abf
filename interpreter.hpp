#ifndef INTERPRETER_HPP
#define INTERPRETER_HPP

#include <vector>
#include <map>

class CodeUnit;

class Interpreter {
	private:
		std::vector<unsigned char> mem;
		std::vector<CodeUnit*> code;
		std::map<std::string, int> labels;
		std::map<std::string, std::string> defines;
		unsigned int codePointer = 0;
		int returnCode = 0;

	public:
		Interpreter();
		bool prepareCode(std::string);
		int getCodeLen();
		CodeUnit* getCodeAt(int);
		bool setCodePointer(unsigned int);
		unsigned int getCodePointer();
		bool setMemAt(int, unsigned char);
		unsigned char getMemAt(int);
		int getLabelAddr(std::string);
		void addMemory(long int);
		void setReturnCode(int);
		int getReturnCode();
		int convertAddr(std::string);
		std::vector <std::string> split(std::string, std::string);
		std::string replaceDefine(std::string);
		bool step();

};

#endif

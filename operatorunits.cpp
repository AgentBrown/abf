#include "operatorunits.hpp"
#include "interpreter.hpp"
#include <vector>
#include <iostream>

OperatorUnit::OperatorUnit(codetype ct, std::string args, Interpreter* parent) : CodeUnit::CodeUnit(ct, args, parent) {};
bool OperatorUnit::execute() {
    CodeUnit::execute();
	std::vector <std::string> args = this->parent->split(this->args, " ");
	if (args.size() != 2) {
		std::cerr << "Runtime error: invalid args for add: '" << this->args << "'.\n";
		return false;
	}
	for (unsigned int i = 0; i < args.size() - 1; i++) {
		if (this->parent->convertAddr(args[i]) == -1) {
			std::cerr << "Runtime error: invalid arg for add: '"  << args[i] << "'.\n";
			return false;
		} else if (this->parent->convertAddr(args[i]) == -2) {
			std::cerr << "Runtime error: out of allocated range.\n";
			return false;
		}
	}
	long int addr1 = this->parent->convertAddr(args[0]);
	long int addr2 = this->parent->convertAddr(args[1]);
	this->parent->setMemAt(addr1, this->doOperation(\
								this->parent->getMemAt(addr1),\
								this->parent->getMemAt(addr2)\
													));
	return true;
}

unsigned char OperatorUnit::doOperation(unsigned char left, unsigned char right) {
	return left;
}

AddUnit::AddUnit (codetype ct, std::string args, Interpreter* parent) : OperatorUnit::OperatorUnit(ct, args, parent) {};
unsigned char AddUnit::doOperation(unsigned char left, unsigned char right) {
	return left + right;
}

SubUnit::SubUnit (codetype ct, std::string args, Interpreter* parent) : OperatorUnit::OperatorUnit(ct, args, parent) {};
unsigned char SubUnit::doOperation(unsigned char left, unsigned char right) {
	return left - right;
}

MulUnit::MulUnit (codetype ct, std::string args, Interpreter* parent) : OperatorUnit::OperatorUnit(ct, args, parent) {};
unsigned char MulUnit::doOperation(unsigned char left, unsigned char right) {
	return left * right;
}

DivUnit::DivUnit (codetype ct, std::string args, Interpreter* parent) : OperatorUnit::OperatorUnit(ct, args, parent) {};
unsigned char DivUnit::doOperation(unsigned char left, unsigned char right) {
	return left / right;
}

